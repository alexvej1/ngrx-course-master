import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {StoreModule, combineReducers} from "@ngrx/store";
import {INITIAL_APPLICATION_STATE} from "./store/application-state";
import {EffectsModule} from "@ngrx/effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";


import {PreloadAllModules, PreloadingStrategy, Route, RouterModule} from "@angular/router";
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';

import * as fromRouter from '@ngrx/router-store';
import {storeFreeze} from "ngrx-store-freeze";

import {CustomRouterStateSerializer} from "./store/utils";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {RecipesModule} from "./recipes/recipes.module";
import {AppRoutingModule} from "./app-routing.module";
import * as fromShoppingList from './shopping-list/store/shopping-list.reducers';
import * as fromAuth from './auth/store/auth.reducers';
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "./shared/shared.module";
import {ShoppingListModule} from "./shopping-list/shopping-list.module";
import {AuthModule} from "./auth/auth.module";
import {CoreModule} from "./core/core.module";
import {AuthEffects} from "./auth/store/auth.effects";

export class AppCustomPreloader implements PreloadingStrategy {
  preload(route: Route, load: Function): Observable<any> {
    return route.data && route.data.preload ? load() : of(null);
  }
}
export const reducers = {
  shoppingList: fromShoppingList.shoppingListReducer,
  auth: fromAuth.authReducer,
  routerReducer: fromRouter.routerReducer
};


export const metaReducers = [storeFreeze];


@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
      HttpClientModule,
      AppRoutingModule,
      SharedModule,
      ShoppingListModule,
      AuthModule,
      CoreModule,
        StoreModule.forRoot(reducers),
        StoreRouterConnectingModule,
      EffectsModule.forRoot([AuthEffects]),
        StoreDevtoolsModule.instrument({maxAge: 25})
    ],
    providers: [
        {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}








