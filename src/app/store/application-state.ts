
import {RouterStateUrl} from "./utils";
import * as fromRouter from '@ngrx/router-store';
import * as fromShoppingList from '../shopping-list/store/shopping-list.reducers';
import * as fromAuth from '../auth/store/auth.reducers';
import * as AuthActions from '../auth/store/auth.actions'


export interface ApplicationState {
  shoppingList: fromShoppingList.State,
  auth: fromAuth.State
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}


export const INITIAL_APPLICATION_STATE: ApplicationState = {
  shoppingList: fromShoppingList.initialState,
  auth: fromAuth.initialState,
  routerReducer: undefined
};
