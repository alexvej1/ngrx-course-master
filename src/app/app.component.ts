import { Component } from '@angular/core';
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  loadedFeature = 'recipe';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyBvXbh7KLwZkp71IKpY-ZJK5pVMywzLHz8",
      authDomain: "ng-recipe-book-e0f4e.firebaseapp.com",
      databaseURL: "https://ng-recipe-book-e0f4e.firebaseio.com",
      projectId: "ng-recipe-book-e0f4e",
      storageBucket: "ng-recipe-book-e0f4e.appspot.com",
      messagingSenderId: "5700506593"
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
